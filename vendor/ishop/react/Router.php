<?php

namespace ishop;

class Router {
    /**
     * @var array
     */
    protected static $routes = [];

    /**
     * @var array
     */
    protected static $route = [];

    /**
     * @param       $regexp
     * @param array $route
     */
    public static function add($regexp, $route = []) {
        self::$routes[$regexp] = $route;
    }

    /**
     * @return array
     */
    public static function getRoutes(): array {
        return self::$routes;
    }

    /**
     * @return array
     */
    public static function getRoute(): array {
        return self::$route;
    }

    /**
     * @param $url
     */
    public static function dispatch($url) {
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            $controller = 'app\controllers\\' . self::$route['prefix'] . self::$route['controller'] . 'Controller';
            if (class_exists($controller)) {
                $controllerObject = new $controller(self::$route);
                $action = 'action' . self::upperCamelCase(self::$route['action']);
                if (method_exists($controllerObject, $action)) {
                    $controllerObject->$action();
                    $controllerObject->getView();
                } else {
                    throw new \Exception('Метод ' . $controller . '::' . $action . ' не найден', 404);
                }
            } else {
                throw new \Exception('Контроллер ' . $controller . ' не найден', 404);
            }
        } else {
            throw new \Exception('Страница не найдена', 404);
        }
    }

    /**
     * @param $url
     * @return bool
     */
    public static function matchRoute($url): bool {
        foreach (self::$routes as $pattern => $route) {
            if (preg_match('#' . $pattern . '#', $url, $matches)) {
                foreach ($matches as $key => $value) {
                    if (is_string($key)) {
                        $route[$key] = $value;
                    }
                }
                if (empty($route['action'])) {
                    $route['action'] = 'index';
                }
                if (!isset($route['prefix'])) {
                    $route['prefix'] = '';
                } else {
                    $route['prefix'] .= '\\';
                }
                $route['controller'] = self::upperCamelCase($route['controller']);
                self::$route = $route;
                return true;
            }
        }

        return false;
    }

    protected static function upperCamelCase($name) {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

    protected static function lowerCamelCase($name) {
        return lcfirst(self::upperCamelCase($name));
    }

    protected static function removeQueryString($url) {
        if ($url) {
            $params = explode('&', $url, 2);
            if (strpos($params[0], '=') === false) {
                return rtrim($params[0], '/');
            } else {
                return '';
            }
        }
    }
}