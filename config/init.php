<?php

define("DEBUG", 1);
define("ROOT", dirname(__DIR__));
define("WWW", ROOT . '/public');
define("APP", ROOT . '/app');
define("REACT", ROOT . '/vendor/ishop/react');
define("LIBS", ROOT . '/vendor/ishop/react/libs');
define("CACHE", ROOT . '/tmp/cache');
define("CONFIG", ROOT . '/config');
define("LAYOUT", 'main');
define("PATH", 'https://' . $_SERVER['HTTP_HOST']);
define("ADMIN", PATH . '/admin');

require_once ROOT . '/vendor/autoload.php';