<?php

namespace app\controllers;

use ishop\Cache;

use ishop\App;
use RedBeanPHP\R;

class MainController extends AppController {
    public function actionIndex() {
        $brands = R::find('brand', 'LIMIT 3');
        $hits = R::find('product', 'status = "1" AND hit = "1" LIMIT 8');
        $this->setMeta('Главная страница', 'Описание', 'Ключевые слова');
        $this->set(compact('brands', 'hits'));
    }
}